# Micro ACL (Laravel / Lumen )

A piece of middleware that can be used to determine of the current user has access to the the requested resource. Tested
on Laravel 5.


## Contents

- [Installation](#installation)



## Installation

In order to install Micro Acl just add

	"repositories": [
	    {
	        "url": "https://charlesjackson@bitbucket.org/tjmorris_web/microacl.git",
	        "type": "git"
	    }
	],

... and ...

    "homebargain/microacl": "dev-master"

to your composer.json. Then run `composer install` or `composer update`.

Then in your `config/app.php` add
```php
    HomeBargain\Microacl\MicroaclServiceProvider::class,
```
in the `providers` array and
```php
    'Microacl'   => HomeBargain\Microacl\MicroaclFacade::class,
```
to the `aliases` array.

If you are going to use [Middleware](#middleware) (requires Laravel 5.1 or later) you also need to add
```php
    'permission' => \HomeBargain\Microacl\Middleware\MicroaclPermission::class,
```
to `routeMiddleware` array in `app/Http/Kernel.php`.
