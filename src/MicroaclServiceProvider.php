<?php namespace HomeBargain\Microacl;

/**
 * This file is part of Microacl
 *
 * @license MIT
 * @package HomeBargain\Microacl
 */

use Illuminate\Support\ServiceProvider;

class MicroaclServiceProvider extends ServiceProvider
{
    /**
     * Indicates if loading of the provider is deferred.
     *
     * @var bool
     */
    protected $defer = false;

    /**
     * Bootstrap the application events.
     *
     * @return void
     */
    public function boot()
    {
        // Publish config files
        $this->publishes([
            __DIR__.'/config/config.php' => config_path('microacl.php'),
        ]);
    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        $this->registerMicroacl();

        $this->mergeConfig();
    }

    /**
     * Register the application bindings.
     *
     * @return void
     */
    private function registerMicroacl()
    {
        $this->app->bind('microacl', function ($app) {
            return new Microacl($app);
        });
        
        $this->app->alias('microacl', 'HomeBargain\Microacl\Microacl');
    }

    /**
     * Merges user's and microacl's configs.
     *
     * @return void
     */
    private function mergeConfig()
    {
        $this->mergeConfigFrom(
            __DIR__.'/../config/config.php', 'microacl'
        );
    }

}