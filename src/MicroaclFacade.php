<?php namespace HomeBargain\Microacl;

/**
 * This file is part of Microacl
 *
 * @license MIT
 * @package HomeBargain\Microacl
 */

use Illuminate\Support\Facades\Facade;

class MicroaclFacade extends Facade
{
    /**
     * Get the registered name of the component.
     *
     * @return string
     */
    protected static function getFacadeAccessor()
    {
        return 'microacl';
    }
}