<?php namespace HomeBargain\Microacl\Middleware;

/**
 * This file is part of Microacl
 *
 * @license MIT
 * @package HomeBargain\Microacl
 */

use Closure;

class MicroaclPermission
{
	protected $auth;

	/**
	 * Creates a new instance of the middleware.
	 */
	public function __construct()
	{
		/* We need to grab and decode the JWT */
		// $this->jwt = 
	}

	/**
	 * Handle an incoming request.
	 *
	 * @param  \Illuminate\Http\Request $request
	 * @param  Closure $next
	 * @param  $permissions
	 * @return mixed
	 */
	public function handle( $request, Closure $next, $permissions )
	{
		if ( $this->auth->guest() || !$request->user()->can(explode('|', $permissions))) {
			abort(403);
		}

		return $next($request);
	}
}