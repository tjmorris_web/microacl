<?php

/**
 * This file is part of Microacl
 *
 * @license MIT
 * @package Homebargain\Microacl
 */

return [

    /*
    |--------------------------------------------------------------------------
    | Microacl Header
    |--------------------------------------------------------------------------
    |
    | The header used to store the ACL rules.
    |
    */
    'permissions_header' => 'X-Acl',

    /*
    |--------------------------------------------------------------------------
    | Microacl Permissions
    |--------------------------------------------------------------------------
    |
    | Name of the JSON file used to store all the user permissions.
    |
    */
    'permissions_file'	 =>	"microacl.json",

    /*
    |--------------------------------------------------------------------------
    | Microacl secret
    |--------------------------------------------------------------------------
    |
    | Name of the JSON file used to store all the user permissions.
    |
    */
    'secret'   => "TGpX5OGfFPoyR1r6qgnzPq6p7A0oIbDz"

]